/**
 * Created by kliko on 6.5.2017 г..
 */
import { Component, OnInit } from '@angular/core';
import { CircuitService } from '../shared/services/circuit.service';
import { Circuit } from '../shared/models/circuit.model';

@Component({
    selector: 'circuit-list',
    templateUrl: './circuit-list.component.html'
})

export class CircuitListComponent implements OnInit {
    public circuits: Circuit[];

    public constructor(private circuitService: CircuitService) {
    }

    public ngOnInit() {
        this.circuitService.index().subscribe((circuits: Circuit[]) => {
            this.circuits = circuits;
        });
    }
}
