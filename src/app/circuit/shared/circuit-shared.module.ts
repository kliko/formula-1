/**
 * Created by kliko on 7.5.2017 г..
 */
import { NgModule } from '@angular/core';
import { CircuitService } from './services/circuit.service';

@NgModule({
    declarations: [],
    imports: [],
    exports: [],
    providers: [
        CircuitService
    ],
})

export class CircuitSharedModule {
}
