/**
 * Created by kliko on 7.5.2017 г..
 */
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Circuit } from '../models/circuit.model';

@Injectable()
export class CircuitService {
    public endpoint: string = 'http://ergast.com/api/f1/2016/';
    public model: any = Circuit;

    public constructor(private http: Http) {
    }

    public index(): Observable<any> {
        return this.http.get(`${this.endpoint}circuits.json`)
            .map((response: any) => {
                response = response.json();
                return response.MRData
                    .CircuitTable
                    .Circuits.map((circuit: any) => new this.model(circuit));
            });
    }
}
