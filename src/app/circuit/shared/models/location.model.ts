/**
 * Created by kliko on 8.5.2017 г..
 */
export class Location {
    public country: string;
    public city: string;

    public constructor(params: any) {
        this.country = params.country;
        this.city = params.locality;
    }
}
