import { Location } from './location.model';
/**
 * Created by kliko on 7.5.2017 г..
 */
export class Circuit {
    public image: string;
    public name: string;
    public url: string;
    public location: Location;

    public constructor(params: any) {
        this.image = `localhost:3000/img/circuits/${params.circuitId}`;
        this.name = params.circuitName;
        this.url = params.url;
        this.location = new Location(params.Location);
    }
}
