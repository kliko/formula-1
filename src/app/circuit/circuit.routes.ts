/**
 * Created by kliko on 6.5.2017 г..
 */
import { CircuitListComponent } from './circuit-list/circuit-list.component';

export const CIRCUIT_ROUTES = [
    {
        path: 'circuits',
        component: CircuitListComponent
    },
];
