/**
 * Created by kliko on 6.5.2017 г..
 */
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { ENV_PROVIDERS } from '../environment';
import { CONSTRUCTOR_ROUTES } from './constructor.routes';
import { ConstructorListComponent } from './constructor-list/constructor-list.component';
import { ConstructorSharedModule } from './shared/constructor-shared.module';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ConstructorListComponent
    ],
    imports: [ // import Angular's modules
        CommonModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(CONSTRUCTOR_ROUTES),
        ConstructorSharedModule
    ],
    providers: [ // expose our Services and Providers into Angular's dependency injection
        ENV_PROVIDERS,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class ConstructorModule {

}