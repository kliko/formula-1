/**
 * Created by kliko on 6.5.2017 г..
 */
import { Component, OnInit } from '@angular/core';
import { ConstructorService } from '../shared/services/constructor.service';
import { Constructor } from '../shared/models/constructor.model';

@Component({
    selector: 'constructor-list',
    templateUrl: './constructor-list.component.html'
})

export class ConstructorListComponent implements OnInit {
    public constructors: Constructor[];

    public constructor(private constructorService: ConstructorService) {
    }

    public ngOnInit() {
        this.constructorService.index().subscribe((constructors: Constructor[]) => {
            this.constructors = constructors;
        });
    }
}
