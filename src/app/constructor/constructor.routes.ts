/**
 * Created by kliko on 6.5.2017 г..
 */
import { ConstructorListComponent } from './constructor-list/constructor-list.component';

export const CONSTRUCTOR_ROUTES = [
    {
        path: 'constructors',
        component: ConstructorListComponent
    },
];
