/**
 * Created by kliko on 7.5.2017 г..
 */
import { NgModule } from '@angular/core';
import { ConstructorService } from './services/constructor.service';

@NgModule({
    declarations: [],
    imports: [],
    exports: [],
    providers: [
        ConstructorService
    ],
})

export class ConstructorSharedModule {
}
