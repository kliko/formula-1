/**
 * Created by kliko on 7.5.2017 г..
 */
export class Constructor {
    public image: string;
    public name: string;
    public nationality: string;
    public nationalityFlag: string;
    public wiki: string;

    public constructor(params: any) {
        console.log(params);
        this.image = `localhost:3000/img/constructors/${params.constructorId}`;
        this.name = params.name;
        this.nationality = params.nationality;
        this.nationalityFlag = `localhost:3000/img/flags/${params.nationality}`;
        this.wiki = params.url;
    }
}
