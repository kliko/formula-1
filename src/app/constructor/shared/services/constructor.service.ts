/**
 * Created by kliko on 7.5.2017 г..
 */
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Constructor } from '../models/constructor.model';

@Injectable()
export class ConstructorService {
    public endpoint: string = 'http://ergast.com/api/f1/2016/';
    public model: any = Constructor;

    public constructor(private http: Http) {
    }

    public index(): Observable<any> {
        return this.http.get(`${this.endpoint}constructors.json`)
            .map((response: any) => {
                response = response.json();
                return response.MRData
                    .ConstructorTable
                    .Constructors
                    .map((constructor: any) => new this.model(constructor));
            });
    }
}
