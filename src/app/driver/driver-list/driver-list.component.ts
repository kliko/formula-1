/**
 * Created by kliko on 6.5.2017 г..
 */
import { Component, OnInit } from '@angular/core';
import { DriverService } from '../shared/services/driver.service';
import { Driver } from '../shared/models/driver.model';

@Component({
    selector: 'driver-list',
    templateUrl: './driver-list.component.html'
})

export class DriverListComponent implements OnInit {
    public drivers: Driver[];

    public constructor(private driverService: DriverService) {
    }

    public ngOnInit() {
        this.driverService.index().subscribe((drivers: Driver[]) => {
            this.drivers = drivers;
        });
    }
}
