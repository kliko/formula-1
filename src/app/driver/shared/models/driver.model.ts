/**
 * Created by kliko on 7.5.2017 г..
 */
export class Driver {
    public id: number;
    public image: string;
    public name: string;
    public nationality: string;
    public nationalityFlag: string;
    public position: number;
    public points: number;
    public wins: number;
    public dateOfBirth: string;
    public permanentNumber: number;
    public Constructor: any;
    public url: string;

    public constructor(params: any) {
        let driver = params.Driver ? params.Driver : params;
        this.id = driver.driverId;
        this.image = `localhost:3000/img/drivers/${driver.driverId}`;
        this.name = `${driver.givenName} ${driver.familyName}`;
        this.nationality = driver.nationality;
        this.nationalityFlag = `localhost:3000/img/flags/${driver.nationality}`;
        this.dateOfBirth = driver.dateOfBirth;
        this.permanentNumber = driver.permanentNumber;
        this.url = driver.url;

        if (params.position) {
            this.position = +params.position;
        }

        if (params.points) {
            this.points = +params.points;
        }

        if (params.wins) {
            this.wins = +params.wins;
        }

        if (params.Constructors) {
            this.Constructor = params.Constructors[0];
        }
    }
}
