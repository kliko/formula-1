/**
 * Created by kliko on 7.5.2017 г..
 */
import { NgModule } from '@angular/core';
import { DriverService } from './services/driver.service';

@NgModule({
    declarations: [],
    imports: [],
    exports: [],
    providers: [
        DriverService
    ],
})

export class DriverSharedModule {
}
