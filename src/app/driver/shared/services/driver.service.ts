/**
 * Created by kliko on 7.5.2017 г..
 */
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Driver } from '../models/driver.model';

@Injectable()
export class DriverService {
    public endpoint: string = 'http://ergast.com/api/f1/2016/';
    public model: any = Driver;

    public constructor(private http: Http) {
    }

    public index(): Observable<Driver[]> {
        return this.http.get(`${this.endpoint}driverStandings.json`)
            .map((response: any) => {
                response = response.json();
                let drivers = [];
                response.MRData
                    .StandingsTable
                    .StandingsLists
                    .map((list: any) => {
                        drivers.push(
                            ...list.DriverStandings.map((driver: any) => new this.model(driver))
                        );
                    });

                return drivers;
            });
    }

    public view(id: string) {
        return this.http.get(`${this.endpoint}drivers/${id}.json`)
            .map((response: any) => {
                response = response.json();
                let driver: any = response.MRData
                    .DriverTable
                    .Drivers[0];

                console.log(driver);

                return new this.model(driver);
            });
    }
}
