/**
 * Created by kliko on 6.5.2017 г..
 */
import { Component, OnInit } from '@angular/core';
import { DriverService } from '../shared/services/driver.service';
import { Driver } from '../shared/models/driver.model';
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: 'driver-preview',
    templateUrl: './driver-preview.component.html'
})

export class DriverPreviewComponent implements OnInit {
    public driver: Driver;

    public constructor(private driverService: DriverService,
                       private activatedRoute: ActivatedRoute) {
    }

    public ngOnInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            this.driverService.view(params.id).subscribe((driver: Driver) => {
                this.driver = driver;
            });
        });
    }
}
