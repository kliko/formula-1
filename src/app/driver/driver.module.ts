/**
 * Created by kliko on 6.5.2017 г..
 */
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { ENV_PROVIDERS } from '../environment';
import { DRIVER_ROUTES } from './driver.routes';
import { DriverListComponent } from './driver-list/driver-list.component';
import { DriverSharedModule } from './shared/driver-shared.module';
import { CommonModule } from '@angular/common';
import { DriverPreviewComponent } from "./driver-preview/driver-preview.component";

@NgModule({
    declarations: [
        DriverListComponent,
        DriverPreviewComponent
    ],
    imports: [ // import Angular's modules
        CommonModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(DRIVER_ROUTES),
        DriverSharedModule
    ],
    providers: [ // expose our Services and Providers into Angular's dependency injection
        ENV_PROVIDERS,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class DriverModule {

}