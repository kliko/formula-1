/**
 * Created by kliko on 6.5.2017 г..
 */
import { DriverListComponent } from './driver-list/driver-list.component';
import { DriverPreviewComponent } from './driver-preview/driver-preview.component';

export const DRIVER_ROUTES = [
    {
        path: '',
        component: DriverListComponent
    },
    {
        path: 'driver/:id',
        component: DriverPreviewComponent
    },
];
