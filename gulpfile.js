/**
 * Created by kliko on 8.5.2017 г..
 */
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    sequence = require('gulp-sequence');

gulp.task('default', sequence('sass', 'less', 'merge-styles'));

gulp.task('sass', function () {
    return gulp.src('src/assets/scss/app.scss')
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest('src/assets/css/scss'));
});

gulp.task('less', function () {
    return gulp.src('src/assets/less/app.less')
        .pipe(less())
        .pipe(gulp.dest('src/assets/css/less'));
});

gulp.task('merge-styles', function () {
    return gulp.src(['src/assets/css/less/app.css', 'src/assets/css/scss/app.css'])
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('src/assets/css'));
});